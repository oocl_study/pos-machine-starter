package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {

        List<ReceiptItem> receiptItems = decodeToItems(barcodes);

        Receipt receipt = calculateCost(receiptItems);

        return renderReceipt(receipt);
    }

    public String renderReceipt(Receipt receipt) {
        return generateReceipt(receipt);
    }

    public String generateReceiptItem(Receipt receipt) {
//        Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        String receiptItemString = "";

        for (ReceiptItem receiptItem : receiptItems) {
            receiptItemString += receiptItem.toString() + '\n';
        }

        return receiptItemString;
    }

    public String generateReceipt(Receipt receipt) {
        return "***<store earning no money>Receipt***\n" +
                generateReceiptItem(receipt) +
                "----------------------\n" +
                "Total: " + receipt.getTotalPrice() + " (yuan)\n" +
                "**********************";
    }


    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        Receipt receipt = new Receipt();
        int totalPrice = calculateTotalPrice(receiptItems);

        receipt.setReceiptItems(receiptItems);
        receipt.setTotalPrice(totalPrice);

        return receipt;
    }

    private int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }


    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> dbItems = ItemsLoader.loadAllItems();

        Map<String, Integer> barcodesMap = new HashMap<>();
        for (String barcode : barcodes) {
            if (barcodesMap.containsKey(barcode)) {
                barcodesMap.put(barcode, barcodesMap.get(barcode) + 1);
            } else {
                barcodesMap.put(barcode, 1);
            }
        }

        List<ReceiptItem> receiptItems = new ArrayList<>();
        for (String barcode : barcodesMap.keySet()) {

            Item dbItem = findItemByBarcode(barcode, dbItems);

            if (dbItem != null) {
                Integer quantity = barcodesMap.get(barcode);
                int unitPrice = dbItem.getPrice();
                ReceiptItem receiptItem = new ReceiptItem(dbItem.getName(), quantity, unitPrice, quantity * unitPrice);
                receiptItems.add(receiptItem);
            }
        }


        return receiptItems;

    }

    public Item findItemByBarcode(String barcode, List<Item> dbItems) {
        for (Item dbItem : dbItems) {
            if (barcode.equals(dbItem.getBarcode())) {
                return dbItem;
            }
        }
        return null;
    }
}
